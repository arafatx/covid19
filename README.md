# covid19

Este repositorio contiene scripts que realizan web scraping y consultas JSON sobre COVID-19, principalmente en Paraguay.

Estos scripts requieren de Python 3 con su biblioteca estándar de módulos (PSL). No utiliza módulos adicionales.

## covid19.py

Este script puede ejecutarse como programa principal o importarse desde otro script.

Al ejecutarse, imprime la cantidad de casos registrados de COVID-19 en el Paraguay, de acuerdo al dato suministrado en el sitio web del MSPyBS.

Ejemplo:

```
$ chmod +x covid19.py
$ ./covid19.py 
Casos registrados: 7
```

Al importarse, ofrece las siguientes funciones:

### navegar(url)
Retorna el contenido de la página referenciada.

```
pagina = navegar("https://www.mspbs.gov.py/covid-19.php")
```

### buscar(cadena, buscada, cantidad)
Retorna una cantidad determinada de caracteres que están a continuación de una buscada.

```
dato = buscar(archivo, "CONTADOR OFICIAL COVID-19 EN PARAGUAY: \r\n\t", 10)
```

### casos()
Retorna la cantidad de casos registrados de COVID-19 en Paraguay.

```
print("Casos registrados:", casos())
```
