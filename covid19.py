#!/usr/bin/env python3
"""
covid19.py - Web scraping de la página con el contador oficial del COVID-19
             del Ministerio de Salud Pública y Bienestar Social del Paraguay.
             Copyleft 2020, Carlos Augusto Zayas Guggiari <czayas en gmail>
"""

import ssl
from urllib.request import urlopen

def navegar(url):
    """Retorna el contenido de la página referenciada."""
    contexto = ssl.create_default_context()
    contexto.check_hostname = False
    contexto.verify_mode = ssl.CERT_NONE
    with urlopen(url, context=contexto) as pagina:
        return pagina.read().decode("utf-8")

def buscar(cadena, buscada, cantidad):
    """Retorna una cantidad de caracteres que le siguen a la buscada."""
    posicion = cadena.find(buscada) + len(buscada)
    return cadena[posicion:posicion+cantidad]

def casos():
    """Retorna la cantidad de casos registrados de COVID-19 en Paraguay."""
    archivo = navegar("https://www.mspbs.gov.py/covid-19.php")
    buscada = "CONTADOR OFICIAL COVID-19 EN PARAGUAY: \r\n\t"
    cadena = buscar(archivo, buscada, 10)
    cantidad = ""
    for caracter in cadena:
        if caracter.isdigit():
            cantidad += caracter
    return int(cantidad)

if __name__ == "__main__":
    print("Casos registrados:", casos())
